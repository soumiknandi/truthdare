var spins = 0;
var people = ["Ankana", "Soumik", "Diganta", "Saikat", "Anirban"];

var count = {
  Ankana: 0,
  Soumik: 0,
  Diganta: 0,
  Saikat: 0,
  Anirban: 0
};

function spin() {
  spins++;
  var board = "";
  var winner = Math.floor(Math.random() * people.length);
  var rotation = spins * 720 + winner * (360 / people.length);
  $("img").css("transform", "rotate(" + rotation + "deg)");
  setTimeout(function() {
    $("output").html(people[winner]);
    count[people[winner]]++;
    Object.keys(count).forEach((element, i) => {
      board += element + " :: " + count[element] + "</br>";
    });
    $("count").html(board);
  }, 3000);
}
